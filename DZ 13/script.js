"use strict";

function changeStyle() {
  const body = document.body;
  body.classList.toggle("dark_theme");
  const section = document.querySelector(".most-popular-posts");
  section.classList.toggle("dark_theme");
  const posts = document.querySelectorAll(".post-description");
  posts.forEach((item) => {
    item.classList.toggle("dark_theme_posts");
  });
  const header = document.querySelector(".nav-menu-header");
  header.classList.toggle("dark_theme");
}

function ready() {
  if (localStorage.getItem("Theme") === "dark") {
    changeStyle();
  } else {
    localStorage.setItem("Theme", "default");
  }
}

document.addEventListener("DOMContentLoaded", ready);
const button = document.querySelector(".change__theme");

function changeTheme(event) {
  event.preventDefault();
  if (localStorage.getItem("Theme") === "default") {
    localStorage.setItem("Theme", "dark");
    changeStyle();
  } else {
    localStorage.setItem("Theme", "default");
    changeStyle();
  }
}

button.addEventListener("click", changeTheme);
