"use strict";

let slides = document.querySelectorAll("#images-wrapper .image-to-show");
console.log(slides);
let currentSlide = 0;
let slideInterval = setInterval(nextSlide, 2000);

function nextSlide() {
  slides[currentSlide].className = "image-to-show";
  currentSlide = (currentSlide + 1) % slides.length;
  slides[currentSlide].className = "image-to-show active";
}
